// Server, mantendra la configuración del servidor.
'use strict'

// Llmar las librerias de body-parser y express
const bodyParcer = require('body-parser');
const express = require('express');

// Iniciamos a express
const app =express();

// Activar los cors;
app.use (function (req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    next();
});

// Activar middlewores de express
app.use(bodyParcer.urlencoded({extended:false}));
app.use(bodyParcer.json());

app.use(require('../Routes/routes'));



module.exports = app;