const mongoose=require('mongoose');

let Schema= mongoose.Schema;
let nuevoProveedorRefaccionaria= new Schema({
    nomProveedor:{type:String},
    appatProveedor:{type:String},
    apmatProveedor:{type:String},
    empresaProveedor:{type:String},
    telProveedor:{type:Number},
});

module.exports=mongoose.model('nuevoProveedor', nuevoProveedorRefaccionaria);