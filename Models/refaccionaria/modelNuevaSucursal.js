const mongoose=require('mongoose');

let Schema= mongoose.Schema;
let nuevaSucursalRefaccionaria= new Schema({
    numSucursal:{type:Number},
    nomSucursal:{type:String},
    locSucursal:{type:String},
    direccionSucursal:{type:String},
    telSucursal:{type:String},
});

module.exports=mongoose.model('nuevaSucursal', nuevaSucursalRefaccionaria);