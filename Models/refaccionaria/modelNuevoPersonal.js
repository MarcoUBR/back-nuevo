const mongoose = require('mongoose');

let Schema = mongoose.Schema;
let nuevoPersonalRefaccionaria = new Schema({
    nomPersonal : {type:String},
    appatPersonal:{type:String},
    apmatPersonal:{type:String},
    edadPersonal:{type:Number},
    telPersonal:{type:Number},
});

module.exports=mongoose.model('nuevoPersonal', nuevoPersonalRefaccionaria);