// Routes va a contener los meotodos GET, POST, DELETE, UPDATE.

const express = require ('express');
const app = express();

app.use(require('./test'));
app.use(require('./productoNuevo/routeNuevoProducto'));
app.use(require('./personalNuevo/routeNuevoPersonal'));
app.use(require('./sucursalNuevo/routeNuevaSucursal'));
app.use(require('./proveedorNuevo/routeNuevoProveedor'));



module.exports = app;