const express=require('express');
const modelPersonalNuevo=require('../../Models/refaccionaria/modelNuevoPersonal');
const app = express();

//!POST
app.post('/personal/nuevo', (req, res)=>{
    let body=req.body;
    console.log(body);

    let newSchemaPersonal=new modelPersonalNuevo({
        nomPersonal : body.nomPersonal,
        appatPersonal:body.appatPersonal,
        apmatPersonal:body.apmatPersonal,
        edadPersonal:body.edadPersonal,
        telPersonal:body.telPersonal,
    });

    newSchemaPersonal
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok:true,
                message: 'Datos guardados exitosamente',
                data
            });
        }
    )
    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok:false,
                message: 'Error al guardar los datos',
                err
            });
        }
    )
});

//! PUT
app.put('/update/personal/:id',async (req,res) => {
    let id = req.params.id;   //constante va al require
    const campos = req.body;  //identifica los campos y elimina la informacion que contiene, mas no el campo
    //LE TURE DEVUELVE DOC ACTUALIZADO ...lo encuentra de acuerdo al id y actualiza los campos
    const respuesta = await modelPersonalNuevo.findByIdAndUpdate(id,campos,{new:true});

    res.status(200).json({
        ok:true,   //
        msj:"Actualización Con Exito",
        respuesta
    });
});


//! GET
app.get('/obtener/personal',async (req,res)=> {
    const respuesta = await modelPersonalNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});



//! DELETE
app.delete('/delete/personal/:id',async (req,res)=>{
    let id = req.params.id;
    const respuesta = await modelPersonalNuevo.findByIdAndDelete(id);   //Aqui se busca  por id y se elimina

    res.status(200).json({
        ok:true,
        msj:"Eliminado Correctamente",
        respuesta
    });
});



module.exports=app;