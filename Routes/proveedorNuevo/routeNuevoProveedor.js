const express=require('express');
const modelProveedorNuevo=require('../../Models/refaccionaria/modelNuevoProveedor');
const app = express();

//!POST
app.post('/proveedor/nuevo', (req, res)=>{
    let body=req.body;
    console.log(body);

    let newSchemaProveedor=new modelProveedorNuevo({
        nomProveedor:body.nomProveedor,
        appatProveedor:body.appatProveedor,
        apmatProveedor:body.apmatProveedor,
        empresaProveedor:body.empresaProveedor,
        telProveedor:body.telProveedor,
    });

    newSchemaProveedor
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok:true,
                message: 'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok:false,
                message: 'No se guardaron los datos',
                err
            });
        }
    )
});

//! PUT
app.put('/update/proveedor/:id',async (req,res) => {
    let id = req.params.id;   //constante va al require
    const campos = req.body;  //identifica los campos y elimina la informacion que contiene, mas no el campo
    //LE TURE DEVUELVE DOC ACTUALIZADO ...lo encuentra de acuerdo al id y actualiza los campos
    const respuesta = await modelProveedorNuevo.findByIdAndUpdate(id,campos,{new:true});

    res.status(200).json({
        ok:true,   //
        msj:"Actualización Exitosa",
        respuesta
    });
});


//! GET
app.get('/obtener/proveedor',async (req,res)=> {
    const respuesta = await modelProveedorNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});



//! DELETE
app.delete('/delete/proveedor/:id',async (req,res)=>{
    let id = req.params.id;
    const respuesta = await modelProveedorNuevo.findByIdAndDelete(id);   //Aqui se busca  por id y se elimina

    res.status(200).json({
        ok:true,
        msj:"Eliminado Correctamente",
        respuesta
    });
});



module.exports=app;