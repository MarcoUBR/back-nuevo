const express=require('express');
const modelSucursalNueva=require('../../Models/refaccionaria/modelNuevaSucursal');
const app = express();

//!POST
app.post('/sucursal/nuevo', (req, res)=>{
    let body=req.body;
    console.log(body);

    let newSchemaSucursal=new modelSucursalNueva({
        numSucursal:body.numSucursal,
        nomSucursal:body.nomSucursal,
        locSucursal:body.locSucursal,
        direccionSucursal:body.direccionSucursal,
        telSucursal:body.telSucursal,
    });

    newSchemaSucursal
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok:true,
                message: 'Datos guardados reina',
                data
            });
        }
    )
    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok:false,
                message: 'No se guardaron los cambios',
                err
            });
        }
    )
});

//! PUT
app.put('/update/sucursal/:id',async (req,res) => {
    let id = req.params.id;   //constante va al require
    const campos = req.body;  //identifica los campos y elimina la informacion que contiene, mas no el campo
    //LE TURE DEVUELVE DOC ACTUALIZADO ...lo encuentra de acuerdo al id y actualiza los campos
    const respuesta = await modelSucursalNueva.findByIdAndUpdate(id,campos,{new:true});

    res.status(200).json({
        ok:true,   //
        msj:"Actualización Exitosa",
        respuesta
    });
});


//! GET
app.get('/obtener/sucursal',async (req,res)=> {
    const respuesta = await modelSucursalNueva.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});



//! DELETE
app.delete('/delete/sucursal/:id',async (req,res)=>{
    let id = req.params.id;
    const respuesta = await modelSucursalNueva.findByIdAndDelete(id);   //Aqui se busca  por id y se elimina

    res.status(200).json({
        ok:true,
        msj:"Eliminado Correctamente",
        respuesta
    });
});



module.exports=app;