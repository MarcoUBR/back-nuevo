const express = require('express');
const modelProductoNuevo = require('../../Models/refaccionaria/modelNuevoProducto');

let app = express();

app.post('/producto/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProducto = new modelProductoNuevo ({
        nombreProducto:  body.nombreProducto, 
        marcaProducto:  body.marcaProducto,
        presentacionProducto:  body.presentacionProducto,
        contenidoProducto:  body.contenidoProducto,
        costoProducto:  body.costoProducto,
        proveedorProducto:  body.proveedorProducto,
        cantidadIngresa:  body.cantidadIngresa,
        statusProducto:  body.statusProducto,
        descripcionProducto:  body.descripcionProducto,
    });

    newSchemaProducto
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok:true,
                message: 'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok:false,
                message: 'No se guardaron los datos',
                err
            });
        }
    )
});

//!PUT
app.put('/update/producto/:id',async (req,res) => {
    let id = req.params.id;   //constante va al require
    const campos = req.body; //identifica los campos y elimina la informacion que contiene, mas no el campo
    //LE TURE DEVUELVE DOC ACTUALIZADO ...lo encuentra de acuerdo al id y actualiza los campos
    const respuesta = await modelProductoNuevo.findByIdAndUpdate(id,campos,{new:true});

    res.status(200).json({
        ok:true,   //
        msj:"Actualización Exitosa",
        respuesta
    });
});

//!GET
app.get('/obtener/producto',async (req,res)=> {
    const respuesta = await modelProductoNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});

//!DELETE
app.delete('/delete/producto/:id',async (req,res)=>{
    let id = req.params.id;
    const respuesta = await modelProductoNuevo.findByIdAndDelete(id);   //Aqui se busca  por id y se elimina

    res.status(200).json({
        ok:true,
        msj:"Eliminación Exitosa",
        respuesta
    });
});

module.exports = app;